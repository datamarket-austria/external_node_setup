# External node setup

This is a short description how the provided docker-compose file can be used to setup all needed components to become
a dynamic data provider (external node), using [assetsRetrieval] and [externalNodeEndpoint].

## Using docker-compose

1. Create needed structure:
    ```
    dma_external_node
    |   assets_retrieval.cfg
    |   assets_retrieval-crontab
    |   external_node.cfg
    |   docker-compose.yml
    |   mapper_dma_place_holder.env
    |_______external_node_endpoint
            |   Dockerfile
            |   id_rsa_assets_db
            |   and other files
    |_______assets_retrieval
            |   Dockerfile
            |   id_rsa_assets_db
            |   and other files
    |_______dma-simple-RML-mapper
            |   Dockerfile
            |   and other files

    ```
    This can be achieved by doing the following:
    1. Create a main DMA folder and put docker-compose file into it
        ```bash
        mkdir dma
        cp /path/to/docker-compose.yml dma_external_node/docker-compose.yml
        ```
    1. Get AssetsDB deploy key and put it into the dma folder
        ```bash
        mv /path/to/id_rsa_deploy_assets_db dma_external_node/id_rsa_assets_db
        ```
    1. Clone all needed packages into this folder (using ssh in this case)
        ```bash
        cd dma_external_node
        git clone git@gitlab.com:datamarket/external_node_endpoint.git
        git clone git@gitlab.com:datamarket/assets_retrieval.git
        git clone git@gitlab.com:datamarket/dma-simple-RML-mapper.git
        ```
    1. Prepare configs  
        Follow the instructions in the [assetsRetrieval] and [externalNodeEndpoint] READMEs to set all needed
        parameters in the configs and move the final configs into the dma_external_node folder.

        Additionally modify mapper_dma_placeholder.env (example in this package) and set all needed environment variables
        for the [Metadata Mapper], description in README.

    If you use a different structure just change the corresponding paths in the docker-compose file.

1. Deploy containers (rebuild them if you changed anything, add ``--build``)
    ```bash
    cd dma_external_node
    docker-compose up
    ```
    All three containers will be up and running. Remember to insert your mapping into the
    [Metadata Mapper]. This could be done using curl:
    ```bash
    curl --data-binary @<mapping-file>.rml.ttl -X POST http://0.0.0.0:8080/RMLMapper/mapper/insertMapping/?temp_id=<mapping_name>
    ```

[externalNodeEndpoint]: https://gitlab.com/datamarket-austria/external_node_endpoint
[assetsRetrieval]: https://gitlab.com/datamarket-austria/assets_retrieval
[Metadata Mapper]: https://gitlab.com/datamarket/dma-simple-RML-mapper
